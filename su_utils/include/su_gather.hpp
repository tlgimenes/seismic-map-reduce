////////////////////////////////////////////////////////////////////////////////
/**
 * @file su_gather.hpp
 * @date 2017-03-06
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2016. All rights reserved.
 *
 * @brief
 *
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef SU_GATHER_HPP
#define SU_GATHER_HPP

////////////////////////////////////////////////////////////////////////////////

#include "su_cdp.hpp"

#include <map>

////////////////////////////////////////////////////////////////////////////////

class su_gather {
  private:
    std::vector<su_cdp> _cdps; // traces grouped by cdp, sorted by traces size
    int _ns;                   // total number of samples
    int _ntrs;
    int _nos;                  // total number of semblances
    int _ttraces;              // total number of traces

  public:
    su_gather(std::string& bin_file_path);

    inline int ns()      { return _ns;                }
    inline int ntrs()    { return _ntrs;              }
    inline int ttraces() { return _ttraces;           }
    inline int ncdps()   { return _cdps.size();       }

    inline su_cdp& operator[](int i) { return _cdps[i]; }
    inline std::vector<su_cdp>& operator()() { return _cdps; }

    void linearize(int* &ntraces_by_cdp_id ,float* & samples, float* &dt, float *&gx, float *&gy, float *&sx, float *&sy, float *&scalco);
};

////////////////////////////////////////////////////////////////////////////////

#endif /*! SU_GATHER_HPP */

////////////////////////////////////////////////////////////////////////////////
