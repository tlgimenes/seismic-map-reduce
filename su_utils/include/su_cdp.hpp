////////////////////////////////////////////////////////////////////////////////
/**
 * @file su_cdp.hpp
 * @date 2017-03-05
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2016. All rights reserved.
 *
 * @brief
 *
 */
////////////////////////////////////////////////////////////////////////////////

#ifndef SU_CDP_HPP
#define SU_CDP_HPP

////////////////////////////////////////////////////////////////////////////////

#include "su_trace.hpp"

#include <vector>

////////////////////////////////////////////////////////////////////////////////

class su_cdp {
  private:
    std::vector<su_trace> _traces;
    int _cdp;

  public:
    su_cdp();

    void push_back(const su_trace& trace);
    inline size_t size() { return _traces.size(); }

    inline const std::vector<su_trace>& traces() const { return _traces; }

    inline int& cdp() { return _cdp; }

    inline bool operator==(const su_cdp& other) const {
      return this->_traces.size() == other.traces().size();
    }

    inline bool operator<(const su_cdp& other) const {
      return _traces.size() > other.traces().size();
    }
};

////////////////////////////////////////////////////////////////////////////////

#endif /*! SU_CDP_HPP */

////////////////////////////////////////////////////////////////////////////////
