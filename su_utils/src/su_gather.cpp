////////////////////////////////////////////////////////////////////////////////
/**
 * @file su_gather.cpp
 * @date 2017-03-06
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2016. All rights reserved.
 *
 * @brief
 *
 * This file contains implementation of the correspoding header file, i.e. .hpp,
 * .hh or .h
 */
////////////////////////////////////////////////////////////////////////////////

#include "su_gather.hpp"

#include "log.hpp"
#include "su_trace.hpp"

#include <algorithm>
#include <cassert>

////////////////////////////////////////////////////////////////////////////////

su_gather::su_gather(std::string& file_path) : 
  _ttraces(0), _nos(0), _ns(-1), _ntrs(0) 
{
  std::ifstream file(file_path, std::ios::binary);
  int min_ns = -1, max_ns = -1, max_ntrs = 0;
  std::map<int, su_cdp> traces_by_cdp;

  for(su_trace tr; tr.fgettr(file);) {
    float hx = tr.halfoffset_x();
    float hy = tr.halfoffset_y();

    if (min_ns < 1) min_ns = tr.ns(); // First trace.
    if (max_ns < 1) max_ns = tr.ns(); // First trace.
    if (min_ns > tr.ns()) min_ns = tr.ns();
    if (max_ns < tr.ns()) max_ns = tr.ns();

    traces_by_cdp[tr.cdp()].push_back(tr);
    max_ntrs = max_ntrs < traces_by_cdp[tr.cdp()].size() ? traces_by_cdp[tr.cdp()].size() : max_ntrs;
  }
  assert(min_ns == max_ns);

  this->_ns = max_ns;
  this->_ntrs = max_ntrs;

  _cdps.reserve(traces_by_cdp.size());
  for(auto& cdp: traces_by_cdp) {
    _cdps.push_back(cdp.second);
    _ttraces += cdp.second.traces().size();
  }
}

////////////////////////////////////////////////////////////////////////////////

void su_gather::linearize(int*& ntraces_by_cdp_id, float *&samples, float *&dt, float* &gx, float* &gy, float* &sx, float* &sy, float* &scalco) {
  gx = new float[_ttraces];
  gy = new float[_ttraces];
  sx = new float[_ttraces];
  sy = new float[_ttraces];
  scalco = new float[_ttraces];
  dt = new float[_cdps.size()];
  samples = new float[_ttraces * _ns];
  ntraces_by_cdp_id = new int[_cdps.size()];

  int n_traces = 0;
  for(int i=0; i < _cdps.size(); i++) {
    dt[i] = _cdps[i].traces()[0].dt();
    ntraces_by_cdp_id[i] = _cdps[i].traces().size();
    ntraces_by_cdp_id[i] += (i > 0) ? ntraces_by_cdp_id[i-1] : 0;

    for(int j=0; j < _cdps[i].traces().size(); j++, n_traces++) {
      assert(n_traces < _ttraces);

      const su_trace& tr = _cdps[i].traces()[j];

      gx[n_traces] = tr.gx();
      gy[n_traces] = tr.gy();
      sx[n_traces] = tr.sx();
      sy[n_traces] = tr.sy();
      scalco[n_traces] = tr.scalco();

      for(int k=0; k < _ns; k++) {
        assert(tr.data().size() == _ns);
        samples[n_traces*_ns + k] = tr.data()[k];
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
