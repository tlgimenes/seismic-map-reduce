////////////////////////////////////////////////////////////////////////////////
/**
 * @file sucut.cpp
 * @date 2017-08-16
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2017. All rights reserved.
 *
 * @brief
 *
 * This file contains implementation of the correspoding header file, i.e. .hpp,
 * .hh or .h
 */
////////////////////////////////////////////////////////////////////////////////

#include "su_gather.hpp"

#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cmath>

////////////////////////////////////////////////////////////////////////////////

void print_help_and_exit() {
  std::cout << "sucut file1 outdir" << std::endl << std::endl;
  std::cout << "This program cuts a file, creating one file per CDP on the outdir parameter";
  std::cout << std::endl << std::endl;
  exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////////////////////

void parse(int argc, const char** argv, std::string& file, std::string& outdir) {
  if(argc != 3) print_help_and_exit();
  
  file = argv[1];
  outdir = argv[2];
}

////////////////////////////////////////////////////////////////////////////////

#define ASSERT(exp) \
    if(!(exp)) { \
      std::cout << "Bad args " << #exp << std::endl; \
      exit(EXIT_FAILURE);\
    }

////////////////////////////////////////////////////////////////////////////////

#define epsilon 1e-15

////////////////////////////////////////////////////////////////////////////////

int main(int argc, const char** argv) {
  std::string path, outdir;

  parse(argc, argv, path, outdir);

  // Creates dir (Unix only)
  std::string createdir = "[ ! -d " + outdir + " ] && mkdir " + outdir;
  system(createdir.c_str());

  // Reads *.su data and starts gather
  su_gather gather(path);
  
  ASSERT(gather.ns() > 0);
  ASSERT(gather.ntrs() > 0);
  ASSERT(gather.ttraces() > 0);
  ASSERT(gather.ncdps() > 0);

  int ns = gather.ns();
  int ncdps = gather.ncdps();
  int ttraces = gather.ttraces();

  for(int cdp_id=0; cdp_id < ncdps; cdp_id++) {
    for(int t=0; t < gather[cdp_id].size(); t++) {
      std::ofstream outpath(outdir + '/' + std::to_string(cdp_id) + ".su", std::ios_base::app);
      su_trace tr = gather[cdp_id].traces()[t];

      tr.fputtr(outpath);
    }
  }

  return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
