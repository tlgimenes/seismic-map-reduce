////////////////////////////////////////////////////////////////////////////////
/**
 * @file main.cpp
 * @date 2017-03-04
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2016. All rights reserved.
 *
 * @brief
 *
 * This file contains implementation of the correspoding header file, i.e. .hpp,
 * .hh or .h
 *
 * TODO:
 *  Try implementing it on OpenCL
 */
////////////////////////////////////////////////////////////////////////////////

#include "su_gather.hpp"

#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cmath>

////////////////////////////////////////////////////////////////////////////////

void print_help_and_exit() {
  std::cout << "surelerr file1 file2" << std::endl << std::endl;
  std::cout << "This program computes the relative error with (file1-file2)/file1";
  std::cout << std::endl << std::endl;
  exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////////////////////

void parse(int argc, const char** argv, std::string& file1, std::string& file2) {
  if(argc != 3) print_help_and_exit();
  
  file1 = argv[1];
  file2 = argv[2];
}

////////////////////////////////////////////////////////////////////////////////

#define ASSERT(exp) \
    if(!(exp)) { \
      std::cout << "Bad args" << #exp << std::endl; \
      exit(EXIT_FAILURE);\
    }

////////////////////////////////////////////////////////////////////////////////

#define epsilon 1e-15

////////////////////////////////////////////////////////////////////////////////

int main(int argc, const char** argv) {
  std::string path1, path2;

  parse(argc, argv, path1, path2);

  // Reads *.su data and starts gather
  su_gather gather1(path1);
  su_gather gather2(path2);

  ASSERT(gather1.ns() == gather2.ns());
  ASSERT(gather1.ntrs() == gather2.ntrs());
  ASSERT(gather1.ttraces() == gather2.ttraces());
  ASSERT(gather1.ncdps() == gather2.ncdps());

  int ns = gather1.ns();
  int ncdps = gather1.ncdps();
  int ttraces = gather1.ttraces();

  int *ntraces_by_cdp_id1;
  int *ntraces_by_cdp_id2;
  float *samples1, *dt1, *gx1, *gy1, *sx1, *sy1, *scalco1;
  float *samples2, *dt2, *gx2, *gy2, *sx2, *sy2, *scalco2;
  double *samples = new double[ttraces*ns];

  gather1.linearize(ntraces_by_cdp_id1, samples1, dt1, gx1, gy1, sx1, sy1, scalco1);
  gather2.linearize(ntraces_by_cdp_id2, samples2, dt2, gx2, gy2, sx2, sy2, scalco2);

  float max = 0;

  for(int i=0; i < ttraces*ns; i++) {
    float haserror = (samples2[i] > 0.90) ? 1.0 : 0.0;
    samples[i] = (samples1[i] - samples2[i]) * haserror;
  }


  for(int i=0; i < ttraces-1; i++) {
    su_trace tr = gather1[0].traces()[0];

    tr.data().assign(samples + i*ns, samples + (i+1)*ns);

    tr.fputtr(std::cout);
  }

  return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
