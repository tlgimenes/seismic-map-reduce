////////////////////////////////////////////////////////////////////////////////
/**
 * @file main.cpp
 * @date 2017-03-04
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2016. All rights reserved.
 *
 * @brief
 *
 * This file contains implementation of the correspoding header file, i.e. .hpp,
 * .hh or .h
 *
 * TODO:
 *  Try implementing it on OpenCL
 */
////////////////////////////////////////////////////////////////////////////////

#include "su_gather.hpp"

#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cmath>

////////////////////////////////////////////////////////////////////////////////

void print_help_and_exit() {
  std::cout << "surelerr file1 rate" << std::endl << std::endl;
  std::cout << "This program subsamples file1 at a fold of size of 1/rate";
  std::cout << std::endl << std::endl;
  exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////////////////////

void parse(int argc, const char** argv, std::string& file, int& fold) {
  if(argc != 3) print_help_and_exit();
  
  file = argv[1];
  fold = std::stoi(argv[2]);
}

////////////////////////////////////////////////////////////////////////////////

#define ASSERT(exp) \
    if(!(exp)) { \
      std::cout << "Bad args " << #exp << std::endl; \
      exit(EXIT_FAILURE);\
    }

////////////////////////////////////////////////////////////////////////////////

#define epsilon 1e-15

////////////////////////////////////////////////////////////////////////////////

int main(int argc, const char** argv) {
  std::string path;
  int fold = 1;

  parse(argc, argv, path, fold);

  // Reads *.su data and starts gather
  su_gather gather(path);
  
  ASSERT(gather.ns() > 0);
  ASSERT(gather.ntrs() > 0);
  ASSERT(gather.ttraces() > 0);
  ASSERT(gather.ncdps() > 0);
  ASSERT(fold > 0);

  int ns = gather.ns();
  int ncdps = gather.ncdps();
  int ttraces = gather.ttraces();

  int *ntraces_by_cdp_id;
  float *samples, *dt, *gx, *gy, *sx, *sy, *scalco;

  gather.linearize(ntraces_by_cdp_id, samples, dt, gx, gy, sx, sy, scalco);

  for(int cdp_id=0; cdp_id < ncdps; cdp_id++) {
    int t0 = (cdp_id > 0 ) ? ntraces_by_cdp_id[cdp_id-1] : 0;
    int tf = ntraces_by_cdp_id[cdp_id];
    int stride = tf - t0;

    for(int t=0; t < fold; t++) {
      su_trace tr = gather[cdp_id].traces()[t%stride];

      tr.fputtr(std::cout);
    }
  }

  return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
