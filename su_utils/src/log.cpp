////////////////////////////////////////////////////////////////////////////////
/**
 * @file log.cpp
 * @date 2017-03-04
 * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)
 *
 * @copyright Tiago Lobato Gimenes 2016. All rights reserved.
 *
 * @brief
 *
 * This file contains implementation of the correspoding header file, i.e. .hpp,
 * .hh or .h
 */
////////////////////////////////////////////////////////////////////////////////

#include "log.hpp"

#include <iostream>
#include <cstdlib>
#include <string>

////////////////////////////////////////////////////////////////////////////////

enum log_level_t logger::_verbosity_level = FAIL;

////////////////////////////////////////////////////////////////////////////////

#define CASE(c, out) \
  case c: if(logger::verbosity_level() >= level) { \
            out << "[" << #c << "]: " << msg << std::endl;\
          } break

void logger::log(enum log_level_t level, const std::string& msg) {
  switch(level) {
    CASE(WARNING, std::cerr);
    CASE(INFO, std::cout);
    CASE(DEBUG, std::cerr);
    default: 
      std::cerr << "[FAIL]: " << msg << std::endl; 
      exit(EXIT_FAILURE);
  }
}

////////////////////////////////////////////////////////////////////////////////

void logger::verbosity_level(int level) {
  switch(level) {
    case 0:  logger ::verbosity_level() = FAIL;    break ;
    case 1:  logger ::verbosity_level() = WARNING; break ;
    case 2:  logger ::verbosity_level() = INFO;    break ;
    default: logger ::verbosity_level() = DEBUG;
  }
}

////////////////////////////////////////////////////////////////////////////////
