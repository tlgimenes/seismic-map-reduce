/**
 * @file	CDP.java
 * @date	Aug 15, 2017
 * @author	Tiago Lobato Gimenes (tlgimenes@gmail.com)
 * 
 * @brief	This is a container for traces. Each trace has an specific CDP given by its cdp id, defined in traces metadata
 */
package com.hpg.seismic;

import java.util.Vector;

import com.hpg.seismic.Trace;

/**
 * @brief This class is a container for the traces
 * @note  This class is just a container and contains the traces as defined in the seismic file
 * @param <T> Float if single, Double if double valued data and computation
 */
public class CDP {
	/**
	 * Linearized structures representing traces. This helps translating to OpenCL
	 */
	private Vector<Float> samples;
	private Vector<Integer> gx, gy, sx, sy;
	private Vector<Short> scalco;
	
	
	/**
	 * Id of this CDP following the order in the seismic file
	 */
	private Integer id;
	
	/**
	 * Number of samples per trace
	 */
	private Short ns;
	
	/**
	 * Time delay
	 */
	private Short dt;
	
	/**
	 * Number of traces
	 */
	private Integer nt;
	
	public CDP() {
		
	}
	
	public CDP(Vector<Trace> traces) {
		this.samples = new Vector<Float>();
		this.gx = new Vector<Integer>();
		this.gy = new Vector<Integer>();
		this.sx = new Vector<Integer>();
		this.sy = new Vector<Integer>();
		this.scalco = new Vector<Short>();
		this.nt = 0;
		
		if(traces != null && traces.size() > 0) {
			id = traces.get(0).getCdpID();
			ns = traces.get(0).getNs();
			dt = traces.get(0).getDt();
			
			for(Trace t: traces) {
				this.addTrace(t);
			}
		} else {
			id = null;
			ns = null;
			dt = null;
		}
	}

	/**
	 * Adds a trace to the collection. This trace needs to have the same cdp id of this cdp
	 * @param trace
	 */
	public void addTrace(Trace trace) {
		// Trace id and CDP id must be the same
		assert(id != null ? trace.getCdpID().intValue() == id.intValue() : true);
		assert((dt != null) ? dt.intValue() == trace.getDt().intValue() : true);
		// Traces must have the same number of samples
		assert((ns != null) ? ns.intValue() == trace.getSamples().size() : true);
		
		nt++;
		
		id = trace.getCdpID();
		ns = trace.getNs();
		dt = trace.getDt();
		
		gx.add(trace.getGx());
		gy.add(trace.getGy());
		sx.add(trace.getSx());
		sy.add(trace.getSy());
		scalco.add(trace.getScalco());
		
		for(Float sample: trace.getSamples()) {
			samples.add(sample);
		}
	}
	
	public Vector<Float> getSamples() {
		return this.samples;
	}
	
	public Vector<Integer> getGx() {
		return this.gx;
	}
	
	public Vector<Integer> getGy() {
		return this.gy;
	}
	
	public Vector<Integer> getSx() {
		return this.sx;
	}
	
	public Vector<Short> getScalco() {
		return this.scalco;
	}
	
	public Vector<Integer> getSy() {
		return this.sy;
	}
	
	public Short getDt() {
		return this.dt;
	}
	
	public Integer getNt() {
		return this.nt;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public Short getNs() {
		return this.ns;
	}
}
