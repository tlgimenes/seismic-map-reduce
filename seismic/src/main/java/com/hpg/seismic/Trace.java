/**
 * @file	Trace.java
 * @date	Aug 15, 2017
 * @author	Tiago Lobato Gimenes (tlgimenes@gmail.com)
 * 
 * @brief	Contains sampĺes and metadata for a single trace
 * 
 * A trace contains meta info + samples array. 
 */
package com.hpg.seismic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

import org.apache.hadoop.io.Writable;

import com.hpg.seismic.Converter;

/**
 * @brief Trace containing trace samples and metadata
 * @note  Contains a samples array and the trace's metadata as defined in the seismic file
 * @param <T> Float in case single, Double in case of double computing
 */
public class Trace implements Writable {
	static int SU_HEADER_SIZE = 240;
	static int SU_CDP_OFFSET = 20; 	/** offset inside header */
	static int SU_SCALCO_OFFSET = 70; /** offset inside header */
	static int SU_SX_OFFSET = 72;	/** offset inside header */
	static int SU_SY_OFFSET = 76;	/** offset inside header */	
	static int SU_GX_OFFSET = 80;	/** offset inside header */
	static int SU_GY_OFFSET = 84;	/** offset inside header */
	static int SU_NS_OFFSET = 114;	/** offset inside header */
	static int SU_DT_OFFSET = 116;	/** offset inside header */
	
	private Vector<Float> samples;
	
	
	/**
	 * CDP ID that this traces belongs to
	 */
	private Integer cdp_id;
	/**
	 * Scale factor
	 */
	private Short scalco;
    private Integer sx;
    private Integer sy;
    private Integer gx;
    private Integer gy;
    /**
     * Time delay
     */
    private Short dt;
    
    /**
     * Number of samples
     */
    private Short ns;
    
    public Trace() { }
    
	public Trace(Vector<Float> samples, Integer cdp_id, Short scalco, Integer sx, Integer sy, Integer gx, Integer gy, Short dt, Short ns) {
		this.samples = samples;
		this.cdp_id = cdp_id;
		this.scalco = scalco;
		this.sx = sx;
		this.sy = sy;
		this.gx = gx;
		this.gy = gx;
		this.dt = dt;
		this.ns = ns;
	}
	
	/**
	 * Builds a Trace for a byte array
	 * @param bytes array representing the trace
	 */
	public Trace(byte[] bytes) {
		this.cdp_id = Converter.readIntFromBuffer(bytes, SU_CDP_OFFSET);
		this.sx = Converter.readIntFromBuffer(bytes, SU_SX_OFFSET);
		this.sy = Converter.readIntFromBuffer(bytes, SU_SY_OFFSET);
		this.gx = Converter.readIntFromBuffer(bytes, SU_GX_OFFSET);
		this.gy = Converter.readIntFromBuffer(bytes, SU_GY_OFFSET);
		this.dt = Converter.readShortFromBuffer(bytes, SU_DT_OFFSET);
		this.scalco = Converter.readShortFromBuffer(bytes, SU_SCALCO_OFFSET);
		this.ns = Converter.readShortFromBuffer(bytes, SU_NS_OFFSET);
		
		this.samples = new Vector<Float>(ns);
		
		// Reads samples
		for(int i=0; i < ns; i++)
			this.samples.add(Float.intBitsToFloat(
					Converter.readIntFromBuffer(bytes, SU_HEADER_SIZE + Float.BYTES * i)));
	}

	public void setHeader(byte[] header) {
		this.cdp_id = Converter.readIntFromBuffer(header, SU_CDP_OFFSET);
		this.sx = Converter.readIntFromBuffer(header, SU_SX_OFFSET);
		this.sy = Converter.readIntFromBuffer(header, SU_SY_OFFSET);
		this.gx = Converter.readIntFromBuffer(header, SU_GX_OFFSET);
		this.gy = Converter.readIntFromBuffer(header, SU_GY_OFFSET);
		this.dt = Converter.readShortFromBuffer(header, SU_DT_OFFSET);
		this.scalco = Converter.readShortFromBuffer(header, SU_SCALCO_OFFSET);
		this.ns = Converter.readShortFromBuffer(header, SU_NS_OFFSET);
	}
	
	public void setSamples(byte[] samples) {
		this.samples = new Vector<Float>(ns);

		// Reads samples
		for(int i=0; i < ns; i++)
			this.samples.add(Float.intBitsToFloat(
					Converter.readIntFromBuffer(samples, Float.BYTES * i)));
	}
	
	public byte[] asByte() {
		// bytes representation of the trace
		byte[] bTrace = new byte[SU_HEADER_SIZE + Float.BYTES * this.ns];
		
		Converter.writeIntToBuffer(bTrace, SU_CDP_OFFSET, this.cdp_id);
		Converter.writeIntToBuffer(bTrace, SU_SX_OFFSET, this.sx);
		Converter.writeIntToBuffer(bTrace, SU_SY_OFFSET, this.sy);
		Converter.writeIntToBuffer(bTrace, SU_GX_OFFSET, this.gx);
		Converter.writeIntToBuffer(bTrace, SU_GY_OFFSET, this.gy);
		Converter.writeShortToByte(bTrace, SU_DT_OFFSET, this.dt);
		Converter.writeShortToByte(bTrace, SU_SCALCO_OFFSET, this.scalco);
		Converter.writeShortToByte(bTrace, SU_NS_OFFSET, this.ns);
		
		for(int it=0; it < this.samples.size(); it++) {
			Converter.writeIntToBuffer(bTrace, SU_HEADER_SIZE + Float.BYTES * it, 
					Float.floatToIntBits(this.samples.get(it)));
		}

		return bTrace;
	}
	
	/**
	 * Get the samples array
	 * @return samples array
	 */
	public Vector<Float> getSamples() {
		return samples;
	}
	
	public Integer getCdpID() {
		return this.cdp_id;
	}
	
	public Short getScalco() {
		return this.scalco;
	}
	
    public Integer getSx() {
    	return this.sx;
    }
    
    public Integer getSy() {
    	return this.sy;
    }
    
    public Integer getGx() {
    	return this.gx;
    }
    
    public Integer getGy() {
    	return this.gy;
    }
    
    public Short getDt() {
    	return this.dt;
    }
    
    public Short getNs() {
    	return this.ns;
    }

	public void readFields(DataInput dataInput) throws IOException {
		// Read number of samples in the array
		this.ns = dataInput.readShort();
		
		// Read bytes from the input
		byte[] tBytes = new byte[Trace.SU_HEADER_SIZE + (this.ns * Float.BYTES)];
		byte[] hBytes = new byte[Trace.SU_HEADER_SIZE];
		byte[] sBytes = new byte[this.ns * Float.BYTES];
		
		// Read all input as a trace byte
		dataInput.readFully(tBytes);
		
		// Breaks trace into header
		for(int it=0; it < Trace.SU_HEADER_SIZE; it++)
			hBytes[it] = tBytes[it];
		
		// Breaks trace into samples
		for(int it=0; it < this.ns * Float.BYTES; it++)
			sBytes[it] = tBytes[it + Trace.SU_HEADER_SIZE]; // copies with offset = Trace.SU_HEADER_SIZE
		
		this.setHeader(hBytes); // set header for this trace
		this.setSamples(sBytes); // set samples for this trace
	}

	public void write(DataOutput dataOutput) throws IOException {
		// Writing the number of samples to the output allows us to know the size of the
		// samples array on the readFields and then read bytes
		dataOutput.writeShort(this.ns); 
		
		// Write this trace as byte to the output
		dataOutput.write(this.asByte());
	}
}

