/**
 * @file	Converter.java
 * @date	Aug 24, 2017
 * @author	Tiago Lobato Gimenes (tlgimenes@gmail.com)
 * 
 * @brief	This class contains definitions for converting primitive data types
 */
package com.hpg.seismic;

/**
 * @brief TODO
 * @note  TODO
 */
public class Converter {
	static int readIntFromBuffer(byte[] buffer, int offset) {
		return (buffer[offset+3]<<24)&0xff000000| (buffer[offset+2]<<16)&0x00ff0000| (buffer[offset+1]<< 8)&0x0000ff00| (buffer[offset]<< 0)&0x000000ff;
	}
	
	static short readShortFromBuffer(byte[] buffer, int offset) {
		return (short) ((short)(buffer[offset+1]<< 8)&0xff00 | (buffer[offset]<< 0)&0x00ff);
	}
	
	static void writeIntToBuffer(byte[] buffer, int offset, int val) {
		for(int i=0; i < Integer.BYTES; i++) {
			buffer[offset+i] = (byte)(val >> (8*i));
		}
	}
	
	static void writeShortToByte(byte[] buffer, int offset, short val) {
		for(int i=0; i < Short.BYTES; i++) {
			buffer[offset+i] = (byte)(val >> (8*i));
		}
	}
}
