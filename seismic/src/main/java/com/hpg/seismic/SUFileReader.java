/**
 * @file	SUInputReader.java
 * @date	Aug 17, 2017
 * @author	Tiago Lobato Gimenes (tlgimenes@gmail.com)
 * 
 * @brief	This file defines a class for reading traces from a file stream
 */
package com.hpg.seismic;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

/**
 * @brief TODO
 * @note  TODO
 */
public class SUFileReader {
	/**
	 * Header size in bytes for each trace. Each trace has a header before its content
	 */
	static int SU_HEADER_SIZE = 240;
	static int SU_CDP_OFFSET = 20; 	/** offset inside header */
	static int SU_SCALCO_OFFSET = 70; /** offset inside header */
	static int SU_SX_OFFSET = 72;	/** offset inside header */
	static int SU_SY_OFFSET = 76;	/** offset inside header */	
	static int SU_GX_OFFSET = 80;	/** offset inside header */
	static int SU_GY_OFFSET = 84;	/** offset inside header */
	static int SU_NS_OFFSET = 114;	/** offset inside header */
	static int SU_DT_OFFSET = 116;	/** offset inside header */
	
	public static Trace readTrace(FileInputStream file) throws IOException {
		byte[] header = new byte[SU_HEADER_SIZE];
		Integer cdp_id, sx, sy, gx, gy;
		Vector<Float> samples;
		Short dt, ns, scalco;
		
		file.read(header);
		
		cdp_id = Converter.readIntFromBuffer(header, SU_CDP_OFFSET);
		sx = Converter.readIntFromBuffer(header, SU_SX_OFFSET);
		sy = Converter.readIntFromBuffer(header, SU_SY_OFFSET);
		gx = Converter.readIntFromBuffer(header, SU_GX_OFFSET);
		gy = Converter.readIntFromBuffer(header, SU_GY_OFFSET);
		dt = Converter.readShortFromBuffer(header, SU_DT_OFFSET);
		ns = Converter.readShortFromBuffer(header, SU_NS_OFFSET);
		scalco = Converter.readShortFromBuffer(header, SU_SCALCO_OFFSET);
		
		byte[] bsamples = new byte[Float.BYTES * ns];
		samples = new Vector<Float>(ns);
		
		file.read(bsamples);
		for(int i=0; i < ns; i++)
			samples.add(Float.intBitsToFloat(Converter.readIntFromBuffer(bsamples, Float.BYTES * i)));
		
		return new Trace(samples, cdp_id, scalco, sx, sy, gx, gy, dt, ns);
	}
}
