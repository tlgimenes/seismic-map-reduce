/**
 * @file	CMPOpenCL.java
 * @date	Aug 16, 2017
 * @author	Tiago Lobato Gimenes (tlgimenes@gmail.com)
 * 
 * @brief	This file includes the OpenCL based implementation of CMP
 */
package com.hpg.seismic;

import com.nativelibs4java.opencl.CLPlatform.DeviceFeature;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Vector;

import org.bridj.Pointer;

import com.nativelibs4java.opencl.*;

/**
 * @brief TODO
 * @note  TODO
 */
public class CMPOpenCL {	
	CLContext ctx;	
	CLQueue queue;
	CLProgram prg;
	
	CLBuffer<Float> hs, cs, samples, num, stt, ctr, str, stk;
	CLBuffer<Integer> gx, gy, sx, sy, scalco;
	
	static Float scaleFactor = (float)1e6;
	
	private Trace semTrace, cTrace, stkTrace;
	
	public CMPOpenCL() throws IOException, CLBuildException {
		// Creates context
		ctx = JavaCL.createBestContext(DeviceFeature.GPU);
		// Creates queue
		queue = ctx.createDefaultQueue();
		// Creates program
		prg = ctx.createProgram(OCLKernelsCMP.source);
		
		// Builds program
		prg.addBuildOption("-DEPSILON=1e-13");
		prg.addBuildOption("-DFACTOR=1e6");
		prg.addBuildOption("-DMAX_W=5");
		prg.build();
	}
	
	protected void createBuffers(CDP cdp, Integer nc) {
		// Creates pointers
		Pointer<Integer> ggx = Pointer.allocateArray(Integer.class, cdp.getGx().size());
		Pointer<Integer> ggy = Pointer.allocateArray(Integer.class, cdp.getGy().size());
		Pointer<Integer> ssx = Pointer.allocateArray(Integer.class, cdp.getSx().size());
		Pointer<Integer> ssy = Pointer.allocateArray(Integer.class, cdp.getSy().size());
		Pointer<Integer> scl = Pointer.allocateArray(Integer.class, cdp.getScalco().size());
		Pointer<Float> ssamples = Pointer.allocateArray(Float.class, cdp.getSamples().size());
		
		// Sets pointers contents
		for(int i=0; i < cdp.getGx().size(); i++) {
			ggx.setIntAtIndex(i, cdp.getGx().get(i));
			ggy.setIntAtIndex(i, cdp.getGy().get(i));
			ssx.setIntAtIndex(i, cdp.getSx().get(i));
			ssy.setIntAtIndex(i, cdp.getSy().get(i));
			scl.setIntAtIndex(i, cdp.getScalco().get(i));
		}
		for(int i=0; i < cdp.getSamples().size(); i++) {
			ssamples.setFloatAtIndex(i, cdp.getSamples().get(i));
		}

		// Creates buffers
		this.gx = ctx.createBuffer(CLMem.Usage.InputOutput, ggx);
		this.gy = ctx.createBuffer(CLMem.Usage.InputOutput, ggy);
		this.sx = ctx.createBuffer(CLMem.Usage.InputOutput, ssx);
		this.sy = ctx.createBuffer(CLMem.Usage.InputOutput, ssy);
		this.scalco = ctx.createBuffer(CLMem.Usage.InputOutput, scl);
		this.samples = ctx.createBuffer(CLMem.Usage.InputOutput, ssamples);

		this.hs = ctx.createFloatBuffer(CLMem.Usage.InputOutput, cdp.getGx().size());
		this.cs = ctx.createFloatBuffer(CLMem.Usage.InputOutput, nc);
		
		this.num = ctx.createFloatBuffer(CLMem.Usage.InputOutput, nc*cdp.getSamples().size());
		this.stt = ctx.createFloatBuffer(CLMem.Usage.InputOutput, nc*cdp.getSamples().size());
		
		this.ctr = ctx.createFloatBuffer(CLMem.Usage.InputOutput, cdp.getSamples().size());
		this.str = ctx.createFloatBuffer(CLMem.Usage.InputOutput, cdp.getSamples().size());
		this.stk = ctx.createFloatBuffer(CLMem.Usage.InputOutput, cdp.getSamples().size());
	}
	
	public void evaluate(CDP cdp, Float c0, Float c1, Short dt, Float tau, Integer nc, Short ns) throws InterruptedException {
		this.createBuffers(cdp, nc);
		
		// Sets hyperparameters for CMP
		Float ddt = (float)(dt) / 1000000.f;
		Float idt = 1.0f/ddt;
		Integer itau = (int)Math.max(tau * idt, 0.0f);
		Integer w = (int)((2*itau) + 1);
		c0 *= CMPOpenCL.scaleFactor;
		c1 *= CMPOpenCL.scaleFactor;
		
		// Builds kernels
		CLKernel kc = prg.createKernel("init_c", cs, (c1-c0)/nc, c0);
		CLKernel khalf = prg.createKernel("init_half", this.scalco, this.gx, this.gy, this.sx, this.sy, this.hs);
		CLKernel ksemblances = prg.createKernel("compute_semblances", this.hs, this.cs, this.samples, this.num, this.stt, cdp.getNt(), idt, ddt, itau, w, nc, new Integer(ns));
		CLKernel kredux = prg.createKernel("redux_semblances", this.cs, this.num, this.stt, this.ctr, this.str, this.stk, nc, new Integer(ns));
		
		// Events handler
		Vector<CLEvent> events = new Vector<CLEvent>();
 		
		// Init C
		events.add(kc.enqueueNDRange(queue, new int[] {nc}));
		
		// Init Half
		events.add(khalf.enqueueNDRange(queue, new int[] {cdp.getNt()}));
		
		// Wait for initialization
		for(CLEvent ev: events) ev.waitFor();
		events.clear();
		
		// Compute Semblances
		events.add(ksemblances.enqueueNDRange(queue, new int[] {ns*nc}));
		
		// Redux semblances
		events.add(kredux.enqueueNDRange(queue, new int[] {ns}, events.lastElement()));
		
		// Wait for computation to finish
		for(CLEvent ev: events) ev.waitFor();
		events.clear();
		
		// Retrieve results from device to host
		Pointer<Float> hctr = Pointer.allocateFloats(ns);
		Pointer<Float> hstk = Pointer.allocateFloats(ns);
		Pointer<Float> hsem = Pointer.allocateFloats(ns);
		events.add(this.ctr.read(queue, hctr, false));
		events.add(this.stk.read(queue, hstk, false));
		events.add(this.str.read(queue, hsem, false));
		
		// Wait for copy to finish
		for(CLEvent ev: events) ev.waitFor();
		
		Vector<Float> ctrSamples = new Vector<Float>(hctr.asList());
		Vector<Float> stkSamples = new Vector<Float>(hstk.asList());
		Vector<Float> semSamples = new Vector<Float>(hsem.asList());
		
		this.cTrace = new Trace(ctrSamples, cdp.getId(), (short) 1, 1, 1, 1, 1, dt, ns);
		this.stkTrace = new Trace(stkSamples, cdp.getId(), (short) 1, 1, 1, 1, 1, dt, ns);
		this.semTrace	= new Trace(semSamples, cdp.getId(), (short) 1, 1, 1, 1, 1, dt, ns);
	}
	
	public Trace getSem() {
		return this.semTrace;
	}
	
	public Trace getC() {
		return this.cTrace;
	}
	
	public Trace getStk() {
		return this.stkTrace;
	}
}
