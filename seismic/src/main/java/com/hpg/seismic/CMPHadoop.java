/**
 * 
 */

package com.hpg.seismic;

import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.nativelibs4java.opencl.JavaCL;

import org.apache.hadoop.mapreduce.JobContext;

public class CMPHadoop extends Configured implements Tool
{	
	private static final Log log = LogFactory.getLog(CMPHadoop.class);
	
	// Sets hyperparameters for CMP method
	private static Integer nc = 101;
	private static Float tau = 0.002f;
	private static Float c0 = (float) 1.98e-7;
	private static Float c1 = (float) 1.77e-6;
	
	public static class SUOutputFormat extends FileOutputFormat<Text, Trace>
	{
		@Override
		public RecordWriter<Text, Trace> getRecordWriter(TaskAttemptContext task)
				throws IOException, InterruptedException 
		{
			return new SURecordWriter(task);
		}
		
		public class SURecordWriter extends RecordWriter<Text, Trace>
		{
			private TaskAttemptContext ctx;
			private HashMap<String, FSDataOutputStream> traceStreamMap;
			
			SURecordWriter(TaskAttemptContext task)
			{
				this.ctx = task;
				this.traceStreamMap = new HashMap<String, FSDataOutputStream>();
			}
			
			@Override
			public void close(TaskAttemptContext task) throws IOException, InterruptedException 
			{
				for(FSDataOutputStream outputStream : this.traceStreamMap.values()) 
				{
					outputStream.close();
				}
			}

			@Override
			public void write(Text traceType, Trace trace) throws IOException, InterruptedException 
			{
				FSDataOutputStream outputStream;
				
				Path outDir = FileOutputFormat.getOutputPath(this.ctx);
				Path outFileName = new Path("/cmp."+traceType.toString()+".su");			
				Path outPath = Path.mergePaths(outDir, outFileName);
				
				log.info("Writting results for: " + trace.getCdpID() + " to file: " + outPath);
				
				FileSystem fs = outPath.getFileSystem(ctx.getConfiguration());
				
				if(fs.exists(outPath))
					outputStream = this.traceStreamMap.get(outFileName.toString());
				else {
					outputStream = fs.create(outPath);
					this.traceStreamMap.put(outFileName.toString(), outputStream);
				}
				
				outputStream.write(trace.asByte());
			}
		}
	}
	
	public static class SUInputFormat extends FileInputFormat<LongWritable, CDP> 
	{		
		@Override
		protected boolean isSplitable(JobContext context, Path filename) {
			return false;
		}

		@Override
		public RecordReader<LongWritable, CDP> createRecordReader(InputSplit arg0, TaskAttemptContext arg1) 
				throws IOException, InterruptedException 
		{
			return new SURecordReader();
		}

		public class SURecordReader extends RecordReader<LongWritable, CDP> 
		{
			private LongWritable key;
			private CDP value;
			private FSDataInputStream inputStream;

			private float progress;
			
			public SURecordReader() 
			{ 
				super();
				
				this.value = null;
				this.key = null;
				this.inputStream = null;
				
				this.progress = 0.0f;
			}

			@Override
			public void close() throws IOException {
				if(this.inputStream != null)
					this.inputStream.close();
			}

			@Override
			public LongWritable getCurrentKey() throws IOException, InterruptedException {
				return key;
			}

			@Override
			public CDP getCurrentValue() throws IOException, InterruptedException {
				return value;
			}

			@Override
			public float getProgress() throws IOException, InterruptedException {
				return this.progress;
			}

			@Override
			public void initialize(InputSplit genericSplit, TaskAttemptContext ctx) throws IOException, InterruptedException 
			{
				Configuration conf = ctx.getConfiguration();
				
				FileSplit fSplit = (FileSplit) genericSplit;
				Path path = fSplit.getPath();
				FileSystem fs = path.getFileSystem(conf);
				
				if(fs.isFile(path))			
					this.inputStream =  path.getFileSystem(conf).open(path);
				
				log.info("CDPRecorder initialization finished");
			}

			public CDP readSUFile() throws IOException 
			{
				// Retrieves the number of samples in a trace
				byte[] header = new byte[Trace.SU_HEADER_SIZE];
				byte[] bSamples = null;
				
				CDP cdp = new CDP(null);
				while(this.inputStream != null && this.inputStream.available() > 0) {
					Trace t = new Trace();
					
					// Reads header from input stream and sets it to trace
					this.inputStream.readFully(header);
					t.setHeader(header);
					
					// Reads samples and sets it to trace
					bSamples = (bSamples == null) ? new byte[t.getNs()*Float.BYTES] : bSamples; 
					this.inputStream.readFully(bSamples);
					t.setSamples(bSamples);
					
					cdp.addTrace(t);
				}
				log.info("READ CDP of id: " + cdp.getId());
				
				return cdp;
			}

			@Override
			public boolean nextKeyValue() throws IOException, InterruptedException 
			{
				log.info("Performing hasNextKeyValue");
				
				if(this.inputStream != null && this.inputStream.available() > 0) {
					this.value = readSUFile();
					this.key = new LongWritable(value.getId());	
					
					this.progress = 1.0f;
					
					return true;
				} 
				return false;
			}
		}
	}

	public static class CMPMapper extends Mapper<LongWritable, CDP, Text, Trace> 
	{	
		public void map(LongWritable cdp_id, CDP cdp, Context context) throws IOException, InterruptedException 
		{
			CMPOpenCL cmp = new CMPOpenCL();

			log.info("Mapper is computing cdp: " + cdp.getId());
			
			cmp.evaluate(cdp, c0, c1, cdp.getDt(), tau, nc, cdp.getNs());
			
			log.info("Mapper finished computing cdp: " + cdp.getId() + ". Emitting key value");
			
			context.write(new Text("c"), cmp.getC());
			context.write(new Text("coher"), cmp.getSem());
			context.write(new Text("stack"), cmp.getStk());
		}
	}

	public static class CMPReducer extends Reducer<Text, Trace, Text, Trace> 
	{
		public void reduce(Text traceType, Iterable<Trace> traces, Context context) throws IOException, InterruptedException
		{
			for(Trace trace : traces)
			{
				context.write(traceType, trace);
			}
			
			log.info("Trace type at reducer: " + traceType);
		}
	}

	public static void main(String[] args) throws Exception 
	{
		System.exit(ToolRunner.run(new Configuration(), new CMPHadoop(), args));
	}

	public int run(String[] args) throws Exception 
	{
		Job job = Job.getInstance(this.getConf(), "CMPHadoop");

		job.setJarByClass(CMPHadoop.class);
		job.setJarByClass(JavaCL.class);
		
		job.setInputFormatClass(SUInputFormat.class);
		job.setOutputFormatClass(SUOutputFormat.class);

		job.setMapperClass(CMPMapper.class);
		job.setReducerClass(CMPReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Trace.class);	

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		log.info("Start Map-Reduce+Hadoop insanity !!");

		return job.waitForCompletion(true) ? 0 : 1;
	}
}