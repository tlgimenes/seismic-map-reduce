/**
 * @file	Kernels.java
 * @date	Sep 1, 2017
 * @author	Tiago Lobato Gimenes (tlgimenes@gmail.com)
 * 
 * @brief	TODO
 */
package com.hpg.seismic;

/**
 * @brief TODO
 * @note  TODO
 */
public class OCLKernelsCMP {
	static final String source = 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"/**\n" + 
" * @file kernels.cl\n" + 
" * @date 2017-04-01\n" + 
" * @author Tiago Lobato Gimenes    (tlgimenes@gmail.com)\n" + 
" *\n" + 
" * @copyright Tiago Lobato Gimenes 2017. All rights reserved.\n" + 
" *\n" + 
" * @brief\n" + 
" *\n" + 
" */\n" + 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"\n" + 
"#ifndef KERNELS_CL\n" + 
"#define KERNELS_CL\n" + 
"\n" + 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"\n" + 
"__kernel void init_c(\n" + 
"        __global float *c,\n" + 
"        float inc,\n" + 
"        float c0\n" + 
"        )\n" + 
"{\n" + 
"    int id = get_global_id(0);\n" + 
"\n" + 
"    c[id] = c0 + inc*id;\n" + 
"}\n" + 
"\n" + 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"\n" + 
"__kernel void init_half(\n" + 
"        __global int *scalco,\n" + 
"        __global int *gx,\n" + 
"        __global int *gy,\n" + 
"        __global int *sx,\n" + 
"        __global int *sy,\n" + 
"        __global float *h)\n" + 
"{\n" + 
"    int i = get_global_id(0);\n" + 
"\n" + 
"    float _s = scalco[i];\n" + 
"\n" + 
"    if(-EPSILON < _s && _s < EPSILON) _s = 1.0f;\n" + 
"    else if(_s < 0) _s = 1.0f / _s;\n" + 
"\n" + 
"    float hx = (gx[i] - sx[i]) * _s;\n" + 
"    float hy = (gy[i] - sy[i]) * _s;\n" + 
"\n" + 
"    h[i] = 0.25 * (hx * hx + hy * hy) / FACTOR;\n" + 
"}\n" + 
"\n" + 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"\n" + 
"__kernel void compute_semblances(\n" + 
"        __global float* h,\n" + 
"        __global float* c,\n" + 
"        __global float* samples,\n" + 
"        __global float* num,\n" + 
"        __global float* stt,\n" + 
"        int nt,\n" + 
"        float _idt,\n" + 
"        float _dt,\n" + 
"        int _tau,\n" + 
"        int _w,\n" + 
"        int nc,\n" + 
"        int ns )\n" + 
"{\n" + 
"    __private float _den = 0.0f, _ac_linear = 0.0f, _ac_squared = 0.0f;\n" + 
"    __private float _num[MAX_W],  m = 0.0f;\n" + 
"    __private int err = 0;\n" + 
"\n" + 
"    __private int i = get_global_id(0);\n" + 
"\n" + 
"    if(i < ns*nc)\n" + 
"    {\n" + 
"        __private int c_id = i % nc;\n" + 
"        __private int t0 = i / nc;\n" + 
"        \n" + 
"        __private float _c = c[c_id];\n" + 
"        __private float _t0 = _dt * t0;\n" + 
"        _t0 = _t0 * _t0;\n" + 
"\n" + 
"        // start _num with zeros\n" + 
"        for(int j=0; j < _w; j++) _num[j] = 0.0f;\n" + 
"\n" + 
"        for(int t_id=0; t_id < nt; t_id++) {\n" + 
"            // Evaluate t\n" + 
"            float t = sqrt(_t0 + _c * h[t_id]);\n" + 
"\n" + 
"            int it = (int)( t * _idt );\n" + 
"            int ittau = it - _tau;\n" + 
"            float x = t * _idt - (float)it;\n" + 
"\n" + 
"            if(ittau >= 0 && it + _tau + 1 < ns) {\n" + 
"                int k1 = ittau + t_id*ns;\n" + 
"                float sk1p1 = samples[k1], sk1;\n" + 
"                for(int j=0; j < _w; j++) {\n" + 
"                    k1++;\n" + 
"                    sk1 = sk1p1;\n" + 
"                    sk1p1 = samples[k1];\n" + 
"\n" + 
"                    // linear interpolation optmized for this problem\n" + 
"                    float v = (sk1p1 - sk1) * x + sk1;\n" + 
"\n" + 
"                    _num[j] += v;\n" + 
"                    _den += v * v;\n" + 
"                    _ac_linear += v;\n" + 
"                }\n" + 
"                m += 1;\n" + 
"            } else { err++; }\n" + 
"        }\n" + 
"\n" + 
"        // Reduction for num\n" + 
"        for(int j=0; j < _w; j++) _ac_squared += _num[j] * _num[j];\n" + 
"		\n" + 
"        // Evaluate semblances\n" + 
"        if(_den > EPSILON && m > EPSILON && _w > EPSILON && err < 2) {\n" + 
"            num[i] = _ac_squared / (_den * m);\n" + 
"            stt[i] = _ac_linear  / (_w   * m);\n" + 
"        }\n" + 
"        else {\n" + 
"            num[i] = -1.0f;\n" + 
"            stt[i] = -1.0f;\n" + 
"        }\n" + 
"    }\n" + 
"}\n" + 
"\n" + 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"\n" + 
"__kernel void redux_semblances(\n" + 
"		__global float *c,\n" + 
"        __global float *num,\n" + 
"        __global float *stt,\n" + 
"        __global float *ctr,\n" + 
"        __global float *str,\n" + 
"        __global float *stk,\n" + 
"        int nc,\n" + 
"        int ns )\n" + 
"{\n" + 
"    __private int t0 = get_global_id(0);\n" + 
"\n" + 
"    __private float max_sem = 0.0f, _num;\n" + 
"    __private int max_c = -1;\n" + 
"\n" + 
"    for(int it=t0*nc; it < (t0+1)*nc ; it++) {\n" + 
"        _num = num[it];\n" + 
"        if(_num > max_sem) {\n" + 
"            max_sem = _num;\n" + 
"            max_c = it;\n" + 
"        }\n" + 
"    }\n" + 
"\n" + 
"    ctr[t0] = max_c > -1 ? c[max_c % nc] : 0;\n" + 
"    str[t0] = max_sem;\n" + 
"    stk[t0] = max_c > -1 ? stt[max_c] : 0;\n" + 
"}\n" + 
"\n" + 
"////////////////////////////////////////////////////////////////////////////////\n" + 
"\n" + 
"#endif /*! KERNELS_CL */\n" + 
"\n" + 
"////////////////////////////////////////////////////////////////////////////////";
}
