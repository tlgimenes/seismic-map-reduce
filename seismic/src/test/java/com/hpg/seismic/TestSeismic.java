package com.hpg.seismic;

import java.io.FileInputStream;
import java.io.IOException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class TestSeismic extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestSeismic( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( TestSeismic.class );
    }

    public void testReadSUFile() {
    	try {
			FileInputStream inputStream = new FileInputStream("../datasets/simple_synthetic/100.su");
			CDP cdp = new CDP(null);

			while(inputStream.available() > 0) {
				Trace t = SUFileReader.readTrace(inputStream);
				
				assert(cdp.getId() != null ? t.getCdpID().intValue() == cdp.getId().intValue() : true);
				assert((cdp.getDt() != null) ? cdp.getDt().intValue() == t.getDt().intValue() : true);
				// Traces must have the same number of samples
				assert((cdp.getNs() != null) ? cdp.getNs().intValue() == t.getSamples().size() : true);
				
				cdp.addTrace(t);
			}
			
			assert(cdp.getNs() == 2502);
			
			inputStream.close();
		} catch(IOException ioe) {
			assert(false);
			ioe.printStackTrace();
		}
    }
    
    public void testTraceByteConvertion() {
    	try {
			FileInputStream inputStream = new FileInputStream("../datasets/simple_synthetic/100.su");
			
			Trace fileTrace = SUFileReader.readTrace(inputStream);
			inputStream.close();
			
			Trace byteTrace = new Trace(fileTrace.asByte());
			
			assert(fileTrace.getCdpID().intValue() == byteTrace.getCdpID().intValue());
			assert(fileTrace.getDt().intValue() == byteTrace.getDt().intValue());
			assert(fileTrace.getGx().intValue() == byteTrace.getGx().intValue());
			assert(fileTrace.getGy().intValue() == byteTrace.getGy().intValue());
			assert(fileTrace.getSy().intValue() == byteTrace.getSy().intValue());
			assert(fileTrace.getSx().intValue() == byteTrace.getSx().intValue());
			assert(fileTrace.getScalco().intValue() == byteTrace.getScalco().intValue());
			assert(fileTrace.getSamples().size() == byteTrace.getSamples().size());
			assert(fileTrace.getSamples().get(fileTrace.getNs()/2).floatValue() == byteTrace.getSamples().get(byteTrace.getNs()/2).floatValue());
			
		} catch(IOException ioe) {
			assert(false);
			ioe.printStackTrace();
		}
    }

    /**
     * Rigourous Test :-)
     */
    public void testCMPOpenCL()
    {
    	try {
			// Opens .su file containing single CDP
			FileInputStream inputStream = new FileInputStream("../datasets/simple_synthetic/100.su");

			// Reads .su file and sets CDP class
			CDP cdp = new CDP(null);
			while(inputStream.available() > 0) {
				Trace t = SUFileReader.readTrace(inputStream);
				
				cdp.addTrace(t);
			}
			inputStream.close();
			
			// Creates CMPOpenCL class
			CMPOpenCL cmp = new CMPOpenCL();
			
			// Sets hyperparameters for CMP method
			Short ns = cdp.getNs();
			Integer nc = 101;
			Short dt = cdp.getDt();
			Float tau = 0.002f;
			Float c0=(float) 1.98e-7;
			Float c1=(float) 1.77e-6;

			// Runs CMP method and returns semblances, C and stack for this cdp as a trace
			cmp.evaluate(cdp, c0, c1, dt, tau, nc, ns);	
			
			Trace semblances = cmp.getSem();
			Trace C = cmp.getC();
			Trace stack = cmp.getStk();
			
			assert(semblances.getNs() == C.getNs());
			assert(stack.getDt() == C.getDt());
		} catch(Exception e) {
			assert(false);
			e.printStackTrace();
		}
    }
    
}
