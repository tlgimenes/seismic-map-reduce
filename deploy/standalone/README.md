- Download hadoop from unicamp mirror. Current version 2.7
- Unpack it in your preferred path. Export HADOOP_HOME to this path
- set JAVA_HOME into your bashrc file
- goto your work location and create a folder called input. Then, move the \*.su files to input.
- Export jar with maven:
  - Enter the seismic folder
  - run `mvn package`
- goto your work location and run the following code
"""
${HADOOP_HOME}/bin/hadoop jar /path/to/exported/jar/seismic-jar-with-dependencies.jar com.hpg.seismic.CMPHadoop input output
"""

Thats it !
